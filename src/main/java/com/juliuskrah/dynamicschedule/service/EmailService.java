package com.juliuskrah.dynamicschedule.service;

import com.juliuskrah.dynamicschedule.model.JobDescriptor;
import com.openco.CronUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.quartz.impl.matchers.GroupMatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Calendar;

import static java.time.ZoneId.systemDefault;
import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobKey.jobKey;
import static org.quartz.TriggerBuilder.newTrigger;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class EmailService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailService.class);

    private final Scheduler scheduler;

    public Date addMinutes(final Date date, final int minutes) {
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MINUTE, minutes);
        return calendar.getTime();
    }

    public Date addDays(final Date date, final int days) {
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, days);
        return calendar.getTime();
    }

    private String generateCronExpression(final Date date) {
        final StringBuilder cron = new StringBuilder();
        try {

            final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            final String dt = dateFormat.format(date);

            final Date cronDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dt);

            final CronUtil calHelper = new CronUtil(cronDate);
            cron.append(calHelper.getMSeconds()).append(" ");
            cron.append(calHelper.getMMins()).append(" ");
            cron.append(calHelper.getMHours()).append(" ");
            cron.append(calHelper.getMDaysOfMonth()).append(" ");
            cron.append(calHelper.getMMonths()).append(" ");
            cron.append(" ? ").append(" "); // days of week.
            cron.append(calHelper.getMYears());

            LOGGER.info("Cron Expression " + cron);

        } catch (final ParseException e) {
            e.printStackTrace();
        }
        return cron.toString();
    }

    public JobDescriptor createJob(String group, JobDescriptor descriptor) {
        // TODO: test dynamic cron expression.
        final String cron = generateCronExpression(addDays(new Date(), 30));
        Set<Trigger> triggersForJob = descriptor.buildTriggers();
        triggersForJob.clear();
        triggersForJob.add(newTrigger()
                .withIdentity(descriptor.getName(), group)
                .withSchedule(cronSchedule(cron)
                        .withMisfireHandlingInstructionFireAndProceed()
                        .inTimeZone(TimeZone.getTimeZone(systemDefault())))
                .usingJobData("cron", cron)
                .build());
        ////////////////////////////////////////////
        descriptor.setGroup(group);
        JobDetail jobDetail = descriptor.buildJobDetail();
//        Set<Trigger> triggersForJob = descriptor.buildTriggers();
        log.info("About to save job with key - {}", jobDetail.getKey());
        try {
            scheduler.scheduleJob(jobDetail, triggersForJob, false);
            log.info("Job with key - {} saved sucessfully", jobDetail.getKey());
        } catch (SchedulerException e) {
            log.error("Could not save job with key - {} due to error - {}", jobDetail.getKey(), e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
        return descriptor;
    }

    @Transactional
    public Optional<List<JobDescriptor>> findJobs(final String group) {
        // @formatter:off
        try {
            final List<JobDescriptor> jobDescriptors = new ArrayList<>();
            for (JobKey jobKey : scheduler.getJobKeys(GroupMatcher.jobGroupEquals(group))) {
                String jobName = jobKey.getName();
                String jobGroup = jobKey.getGroup();

                //get job's detail.
                JobDetail jobDetail = scheduler.getJobDetail(jobKey(jobName, jobGroup));

                //get job's trigger
                List<Trigger> triggers = (List<Trigger>) scheduler.getTriggersOfJob(jobKey);

                jobDescriptors.add(JobDescriptor.buildDescriptor(jobDetail, triggers));

//                Date nextFireTime = triggers.get(0).getNextFireTime();
//
//                System.out.println("[jobName] : " + jobName + " [groupName] : "
//                        + jobGroup + " - " + nextFireTime);
            }
            return Optional.of(jobDescriptors);
        } catch (final SchedulerException e) {
            log.error("Could not find jobs with key - {} due to error - {}", group, e.getLocalizedMessage());
        }
        // @formatter:on
        log.warn("Could not find jobs with key - {}", group);
        return Optional.empty();
    }

    @Transactional
    public Optional<JobDescriptor> findJob(String group, String name) {
        // @formatter:off
        try {
            JobDetail jobDetail = scheduler.getJobDetail(jobKey(name, group));
            if(Objects.nonNull(jobDetail))
                return Optional.of(
                        JobDescriptor.buildDescriptor(jobDetail,
                                scheduler.getTriggersOfJob(jobKey(name, group))));
        } catch (SchedulerException e) {
            log.error("Could not find job with key - {}.{} due to error - {}", group, name, e.getLocalizedMessage());
        }
        // @formatter:on
        log.warn("Could not find job with key - {}.{}", group, name);
        return Optional.empty();
    }

    public void updateJob(String group, String name, JobDescriptor descriptor) {
        try {
            JobDetail oldJobDetail = scheduler.getJobDetail(jobKey(name, group));
            if(Objects.nonNull(oldJobDetail)) {
                JobDataMap jobDataMap = oldJobDetail.getJobDataMap();
                jobDataMap.put("subject", descriptor.getSubject());
                jobDataMap.put("messageBody", descriptor.getMessageBody());
                jobDataMap.put("to", descriptor.getTo());
                jobDataMap.put("cc", descriptor.getCc());
                jobDataMap.put("bcc", descriptor.getBcc());
                JobBuilder jb = oldJobDetail.getJobBuilder();
                JobDetail newJobDetail = jb.usingJobData(jobDataMap).storeDurably().build();
                scheduler.addJob(newJobDetail, true);
                log.info("Updated job with key - {}", newJobDetail.getKey());
                return;
            }
            log.warn("Could not find job with key - {}.{} to update", group, name);
        } catch (SchedulerException e) {
            log.error("Could not find job with key - {}.{} to update due to error - {}", group, name, e.getLocalizedMessage());
        }
    }

    public void deleteJob(String group, String name) {
        try {
            scheduler.deleteJob(jobKey(name, group));
            log.info("Deleted job with key - {}.{}", group, name);
        } catch (SchedulerException e) {
            log.error("Could not delete job with key - {}.{} due to error - {}", group, name, e.getLocalizedMessage());
        }
    }

    public void pauseJob(String group, String name) {
        try {
            scheduler.pauseJob(jobKey(name, group));
            log.info("Paused job with key - {}.{}", group, name);
        } catch (SchedulerException e) {
            log.error("Could not pause job with key - {}.{} due to error - {}", group, name, e.getLocalizedMessage());
        }
    }

    public void resumeJob(String group, String name) {
        try {
            scheduler.resumeJob(jobKey(name, group));
            log.info("Resumed job with key - {}.{}", group, name);
        } catch (SchedulerException e) {
            log.error("Could not resume job with key - {}.{} due to error - {}", group, name, e.getLocalizedMessage());
        }
    }
}
