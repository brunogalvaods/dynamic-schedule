package com.openco;

import lombok.Data;

import java.util.Calendar;
import java.util.Date;

@Data
public class CronUtil {

    private final Date mDate;
    private final Calendar mCal;
    private final String mSeconds = "0";
    private final String mDaysOfWeek = "?";

    private String mMins;
    private String mHours;
    private String mDaysOfMonth;
    private String mMonths;
    private String mYears;

    public CronUtil(Date pDate) {
        this.mDate = pDate;
        mCal = Calendar.getInstance();
        this.generateCronExpression();
    }

    private void generateCronExpression() {
        mCal.setTime(mDate);

        this.mHours = String.valueOf(mCal.get(Calendar.HOUR_OF_DAY));

        this.mMins = String.valueOf(mCal.get(Calendar.MINUTE));

        this.mDaysOfMonth = String.valueOf(mCal.get(Calendar.DAY_OF_MONTH));

        this.mMonths = new java.text.SimpleDateFormat("MM").format(mCal.getTime());

        this.mYears = String.valueOf(mCal.get(Calendar.YEAR));

    }
}
